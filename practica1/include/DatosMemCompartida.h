#pragma once

#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida
{
	public:
	Esfera esfera;
	Raqueta raqueta1,raqueta2;	
	int accion; //abajo=-1, nada=0, arriba=1
};
