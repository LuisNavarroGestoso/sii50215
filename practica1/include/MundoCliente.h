
#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)

#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_
#if _MSC_VER > 1000

#pragma once
#endif // _MSC_VER > 1000

#include<stdio.h>
#include <vector>
#include "Plano.h"
#include "Esfera.h"
#include "Raqueta.h"

//bibliotecas usadas por las tuberías:
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

//bibliotecas empleadas por la memoria compartida:

#include "DatosMemCompartida.h"
#include <sysmman.h>

//bibliotecas para el uso del cliente-servidor:
#include<pthread.h>

class Mundo
{
	public:
		void Init();
		CMundo();
		virtual~CMundo();

	
		void InitGL();
		void OnKeyboardDown(unsigned char key, intx, int y);
		void OnTimer(int value);
		void OnDraw();

		Esfera esfera;
		std::vector<Plano> paredes;
		Plano fondo_izq;
		Plano fondo_dcho;
		Raqueta jugador1;
		Raqueta jugador2;
		
		int puntos1;
		int puntos2;

		//formación de la tubería cliente-servidor:
		int tuberiacs;
		int tuberiateclas;

		//formación atributo memoria compartida:
		DatosMemCompartida MemComp;
		DatosMemCompartida *pMemComp;
};


#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
