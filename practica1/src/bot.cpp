#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include"Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"

int main()
{
	int file;
	DatosMemCompartida *pMemComp;
	char *proyeccion;

	file=open("/tmp/datosBot.txt",O_RDWR);

	proyeccion=(char*)mmap(NULL, sizeof(*(pMemComp)),PROT_WRITE|PROT_READ, MAP_SHARED, file,0);

	close(file);

	pMemComp=(DatosMemCompartida*)proyeccion;

	int abortar=0;

	while(abortar==0)
	{
		if(pMemComp->accion=5)	abortar=1;
				
		usleep(2500);		
		
		float posicionRaqueta;
		
		posicionRaqueta=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
	
		if(posicionRaqueta<pMemComp->esfera.centro.y)
			pMemComp->accion=1;
		
		else if(posicionRaqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;
	
		else	
			pMemComp->accion=0;	
	}	
	
	//desmontar proyeccion memoria:	
	munmap(proyeccion,sizeof(*pMemComp));
}

