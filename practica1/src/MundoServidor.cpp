// Mundo.cpp: implementation of the CMundo class.
//LuisNavarro
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include<iostream>
#include<pthread.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void hilo_comandos(void *d)
{
	CMundo *p=(CMundo *) d;
	p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
	while(1)
	{
		usleep(10);
		char mensaje[150];
		read(tuberiateclas,mensaje,sizeof(mensaje));
		unsigned char key;
		sscanf(mensaje,"%c",&key);
		if(key=='s') 
			jugador1.velocidad.y=-4;
		if(key=='w') 
			jugador1.velocidad.y=4;
		if(key=='l') 
			jugador2.velocidad.y=-4;
		if(key=='o') 
			jugador2.velocidad.y=4;
	}
}
CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//Tuberia:
	char cerrar[250];
	sprintf(cerrar,"FIN DEL JUEGO");
	write(tuberia,cerrar,strlen(cerrar)+1);
	close(tuberia); //termina el proceso de intercambio de info

	close(tuberiacs);
	close(tuberiateclas);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	extern int aux1;	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		//mensaje de la tuberia:
		char mensaje[250];
		sprintf(mensaje,"Jugador 2 consigue 1 punto, tiene 			%d",puntos2);
		write(tuberia,mensaje,strlen(mensaje)+1);
	}

	

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	
		//mensaje de la tuberia:
		char mensaje[250];
		sprintf(mensaje,"Jugador 1 consigue 1 punto, tiene 			%d",puntos1);
		write(tuberia,mensaje,strlen(mensaje)+1);
	}

	if(jugador1.Rebota(esfera))
	{
		jugador1.y1-=0.4;	//disminuye - TAMAÑO "y" raqueta1
		jugador2.y1+=0.4;	//aumenta + TAMAÑO "y" raqueta2
	}

	if(jugador2.Rebota(esfera))
	{
		jugador1.y1+=0.4;	//aumenta + TAMAÑO "y" raqueta1
		jugador2.y1-=0.4;	//disminuye - TAMAÑO "y" raqueta2
	}

	//SECCION CONTROL BOT:
	//transferir los valores actualizados del fichero:
	pMemComp-> esfera=esfera;
	pMemComp-> raqueta1=jugador1;
	pMemComp-> raqueta2=jugador2;

	switch(pMemComp->accion)
	{	
		case -1: jugador1.velocidad.y=-4;
			break;
		case 0: break;
		case 1: jugador1.velocidad.y=+4;
	}

	/*if(aux1==0)
	{
		switch(pMemComp->accion2)
		{	
			case -1: jugador2.velocidad.y=-4;
			break;
			case 0: break;
			case 1: jugador2.velocidad.y=+4;
		}
	}*/
	
	int estadoActual=0, estadoAnterior=0;
	char texto[250];
	
	if(puntos1==4)
	{
		estadoActual=1;
		/*if(estadoActual!=estadoAnterior)
		{
			sprintf(texto,"Jugador 1 gana la partida, tiene 4 puntos");
			write(tuberia,texto,strlen(texto)+1);
		}*/
		estadoAnterior=estadoActual;
		exit(0);
	}

	if(puntos2==4)
	{
		estadoActual=1;
		/*if(estadoActual!=estadoAnterior)
		{
			sprintf(texto,"Jugador 2 gana la partida, tiene 4 puntos");
			write(tuberia,texto,strlen(texto)+1);
		}*/
		estadoAnterior=estadoActual;
		exit(0);
	}	
	
	char comunicacion[250];
	sprintf(comunicacion,"%f %f %f %f %f %f %f %f %f %f %d %d",esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador2.x1,jugador2.y1,jugador1.x2,jugador1.y2,jugador2.x2,jugador2.y2);
	std::cout<<"CS: " << comunicacion<< "n\";
	write(tuberiacs,comunicacion,sizeof(comunicacion));

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	extern int aux1;
		
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;
		//aux1=1; 
		//alarm(5);		
			break;
	case 'o':jugador2.velocidad.y=4;
		//aux1=1;
		//alarm(5);
		break;
	}
}

void CMundo::Init()
{
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	tuberia=open("/tmp/loggerfifo",O_WRONLY);

//fichero proyectado en memoria:
	//creacion:
	int file=open("/tmp/datosBot.txt",O_RDWR | O_CREAT| O_TRUNC, 0777);
	write(file,&MemComp,sizeof(MemComp));
	
	// asignacion proyeccion del fichero al puntero:
	proyeccion=(char*)mmap(NULL,sizeof(MemComp),PROT_WRITE| PROT_READ,MAP_SHARED, file,0);

	close(file);
	
	//copiar valor de proyeccion al puntero de datos
	pMemComp=(DatosMemCompartida*)proyeccion;
	
	pMemComp->accion=0;	//por defecto no existe accion
	tuberiacs=open("/tmp/csfifo".O_WRONLY);
	pthread_create(&thid1,NULL,hilo_comandos,this);
	tuberiateclas=open("/tmp/teclasfifo",O_RDONLY);

}
