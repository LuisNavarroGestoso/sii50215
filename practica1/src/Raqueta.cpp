// Raqueta.cpp: implementation of the Raqueta class.
//LuisNavarro
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	centro.x=0;
	centro.y=0;
	velocidad.x=0;
	velocidad.y=0;	
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	x1+=velocidad.x*t;
	x2+=velocidad.x*t;
	y1+=velocidad.y*t;
	y2+=velocidad.y*t;
	
}
