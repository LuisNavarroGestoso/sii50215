#include<stdio.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>

int main(int argc, char*argv[])
{

	int condicion=0, aux;
	
	mkfifo("/tmp/loggerfifo",0777);
	int fd=open("/tmp/loggerfifo",O_RDONLY);

	while(condicion==0)
	{
		char buffer[250];
		aux=read(fd, buffer, sizeof(buffer));
		printf("%s\n",buffer);
		
		//evitamos errores de lectura
		if((buffer[0]=='-') || (aux==1))
		{	
			printf("Fin del Juego, Cerrando logger\n");

			condicion=1;
		}
	}
	
	close (fd);
	unlink ("/tmp/loggerfifo");
	return 0;

}
	
